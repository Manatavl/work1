﻿using System.Collections.Generic;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class NewEmployer
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        public Guid RolesID { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}
