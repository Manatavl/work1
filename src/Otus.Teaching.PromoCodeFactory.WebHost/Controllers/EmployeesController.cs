using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }
        /// <summary>
        /// удалить по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteByID(Guid id)
        {
            try
            {
                //проверяем, может уже удален
                var employee = await _employeeRepository.GetByIdAsync(id);

                if (employee == null)
                {
                    return NotFound();
                }

                await _employeeRepository.DeleteById(id);

            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Employee deleting error");
            }

            return Ok();
        }

        /// <summary>
        /// Добавить сотрудника
        /// </summary>
        /// <returns></returns>

        [HttpPost("item:Employee")]
        public async Task<IActionResult> CreateEmployee(Employee item)
        {
            try
            {
                await _employeeRepository.AddAsync(item);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Employee creating error");
            }
            return Ok();
        }

        /// <summary>
        /// Редактировать данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<ActionResult> UpdateEmployee(Guid id, NewEmployer employee)
        {
            try
            {
                var employees = await _employeeRepository.GetAllAsync();
                Employee item = employees.FirstOrDefault(t => t.Id == id);

                if (item == null)
                {
                    item = new Employee
                    {
                        Id = Guid.NewGuid()
                    };
                }
                
                item.FirstName = employee.FirstName;
                item.LastName = employee.LastName;
                item.Email = employee.Email;
                item.AppliedPromocodesCount = employee.AppliedPromocodesCount;
                item.Roles.Clear();
                item.Roles = _roleRepository.GetAllAsync().Result.
                    Where(t => t.Id == employee.RolesID).ToList();

                await _employeeRepository.Edit(item);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Employee editing error");
            }
            return Ok();

        }



    }
}
