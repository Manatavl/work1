﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }
        public Task DeleteById(Guid id)
        {
            var list = Data.ToList();

            //если нашли
            var listItem = list.FirstOrDefault(t => t.Id == id);

            if (listItem != null)
            {
                list.Remove(listItem);
            }
            Data = list.AsEnumerable();

            return Task.FromResult(Data);
        }
        
        public Task AddAsync(T item)
        {
            //не удаляем, а создаем новую коллекцию, где этого элемента нет
            var list = Data.ToList();

            //если уже есть такой Id ->  не добавлять
            var listItem = list.FirstOrDefault(t => t.Id == item.Id);

            if (listItem == null)
            {
                list.Add(item);
            }
            Data = list.AsEnumerable();
            return Task.FromResult(Data);
        }

        public Task Edit(T item)
        {
            var list = Data.ToList();

            //если уже есть такой Id ->  не добавлять
            var entity = Data.FirstOrDefault(x => x.Id == item.Id);
            if (entity == null)
            {
                entity = (item);
            }

            Data = list.AsEnumerable();
            return Task.FromResult(Data);
        }
    }
}